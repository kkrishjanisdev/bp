<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>/assets/css/style.css" />
    <title>BP uzdevums</title>
</head>

<body>
    <div class="image-collage">
        <?php
        if (empty($imageCollage)) {
            echo "Nav pievienota neviena bilde!";
        } else {
            foreach ($imageCollage as $image) {
                echo "<img src='" . BASE_URL . '/' . $image->getPath() . "' width='362' height='544' />";
            }
        }
        ?>
    </div>
</body>

</html>