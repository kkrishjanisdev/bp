## Lai palaistu projektu

- Jābūt uzinstalētam Composer. Lai iegūtu Composer: https://getcomposer.org/download/
- Jābūt uzstādītam un palaistam Apache

## Instalācija

- `git clone https://gitlab.com/kkrishjanisdev/bp.git`
- Palaist komandu projekta direktorijā:
  `composer dump-autoload`
