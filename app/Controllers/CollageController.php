<?php

namespace App\Controllers;

use App\Helpers\View;
use App\Models\ImageCollage;

class CollageController
{
    public function index(): string
    {
        return View::render('home.php', ['imageCollage' => ImageCollage::generate()->sortByName()->getImages()]);
    }
}
