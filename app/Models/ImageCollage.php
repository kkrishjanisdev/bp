<?php

namespace App\Models;

class ImageCollage
{
    private $images = [];

    public function addImage(Image $image)
    {
        $this->images[] = $image;
    }

    public function getImages(): array
    {
        return $this->images;
    }

    public static function generate(): ImageCollage
    {
        $collage = new ImageCollage();

        // Izveidojam bilžu masīvu, kur tiek iekļautas mūsu 10 bildes
        for ($i = 1; $i <= 10; $i++) {
            $collage->addImage(new Image('assets/' . $i . '.png'));
        }

        return $collage;
    }

    // Kārtojam bildes pēc nosaukuma
    public function sortByName(): ImageCollage
    {
        usort($this->images, [Image::class, 'compareImages']);

        return $this;
    }
}
