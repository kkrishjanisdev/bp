<?php

namespace App\Models;

class Image
{
    private $path;

    public function __construct($path)
    {
        $this->path = $path;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    // Salīdzinam bildes priekš atlases
    public static function compareImages($a, $b): int
    {
        return strnatcmp($a->getPath(), $b->getPath());
    }
}
